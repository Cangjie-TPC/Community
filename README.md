### Cangjie-TPC

#### 介绍

Cangjie-TPC（Third Party Components）用于汇集基于仓颉编程语言开发的开源三方库，帮助开发者方便、快捷、高质量构建仓颉程序。

欢迎您使用及参与贡献。

#### 贡献三方库
+ [贡献流程](https://gitcode.com/Cangjie-TPC/TPC-Resource/blob/main/HowToContribute.md)

#### 已有三方库汇总

+ [已有三方库资源汇总](https://gitcode.com/Cangjie-TPC/TPC-Resource/overview)

#### 招募三方库汇总
+ [招募三方库资源汇总](https://gitcode.com/Cangjie-TPC/TPC-Resource/blob/main/TPC-LIST.md)

#### 代码仓地址
+ Cangjie主库组织地址：https://gitcode.com/Cangjie

+ Cangjie-SIG组织地址：https://gitcode.com/Cangjie-SIG

+ Cangjie三方库组织地址：https://gitcode.com/Cangjie-TPC

+ Cangjie归档组织地址：https://gitcode.com/Cangjie-Retired

#### 合作联系
官方邮箱：cangjie@huawei.com

三方库合作：[@Climber123](https://gitcode.com/Climber123), [dengkai1@huawei.com](dengkai1@huawei.com)(优先联系) | [@masaijun](https://gitcode.com/masaijun), [masaijun@huawei.com](masaijun@huawei.com) | [@cangjie-wangyanji](https://gitcode.com/cangjie_wangyanji), [wangyanji1@huawei.com](wangyanji1@huawei.com)

